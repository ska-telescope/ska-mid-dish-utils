from functools import partial
import os
import time
import datetime
import numpy as np
import matplotlib.pyplot as plt
from tango import DeviceProxy, EventType
from astropy.time import Time
from asyncua.sync import Client


class PointingGrapher:
    def __init__(
        self,
        dish_manager_device="mid-dish/dish-manager/ska001",
        ds_manager_device="mid-dish/ds-manager/ska001",
        dsc_fqdn="opc.tcp://ds-opcua-server-simulator-001-svc:4840/dish-structure/server/"
    ):
        self.tango_devices = {
            "DishManager": {
                "proxy": None,
                "az_timestamps": [],
                "el_timestamps": [],
                "az_values": [],
                "el_values": [],
            },
            "DSManager": {
                "proxy": None,
                "az_timestamps": [],
                "el_timestamps": [],
                "az_values": [],
                "el_values": [],
            },
        }

        self.opcua_az_timestamps = []
        self.opcua_az_values = []
        self.opcua_el_timestamps = []
        self.opcua_el_values = []

        # Connect to the Tango device
        if dish_manager_device is not None:
            self.tango_devices["DishManager"]["proxy"] = DeviceProxy(dish_manager_device)
        if ds_manager_device is not None:
            self.tango_devices["DSManager"]["proxy"] = DeviceProxy(ds_manager_device)

        self.opcua_namespace = "http://skao.int/DS_ICD/"

        self.opcua_client = Client(url=dsc_fqdn) if dsc_fqdn is not None else None

    def begin(self, show_graph=False):
        if self.opcua_client is not None:
            self.opcua_client.connect()
            self.opcua_client.load_data_type_definitions()
            idx = self.opcua_client.get_namespace_index(uri=self.opcua_namespace)

            self.plc_prg_node = self.opcua_client.nodes.root.get_child(
                [
                    "0:Objects",
                    f"{idx}:Logic",
                    f"{idx}:Application",
                    f"{idx}:PLC_PRG",
                ]
            )
            self.az_p_act_node = self.plc_prg_node.get_child(
                [
                    f"{idx}:Azimuth",
                    f"{idx}:p_Act",
                ]
            )
            self.el_p_act_node = self.plc_prg_node.get_child(
                [
                    f"{idx}:Elevation",
                    f"{idx}:p_Act",
                ]
            )

            az_handler = SubscriptionHandler(self.az_p_act_change_cb)
            el_handler = SubscriptionHandler(self.el_p_act_change_cb)

            # 50ms polling rate
            az_subscription = self.opcua_client.create_subscription(50, az_handler)
            el_subscription = self.opcua_client.create_subscription(50, el_handler)

            # We subscribe to data changes for two nodes (variables).
            az_subscription.subscribe_data_change(self.az_p_act_node)
            el_subscription.subscribe_data_change(self.el_p_act_node)

        if self.tango_devices["DishManager"]["proxy"] is not None:
            subscription_id_dm = self.tango_devices["DishManager"]["proxy"].subscribe_event(
                "achievedPointing",
                EventType.CHANGE_EVENT,
                partial(self.achieved_pointing_change_event_callback, "DishManager"),
            )
        if self.tango_devices["DSManager"]["proxy"] is not None:
            subscription_id_dsm = self.tango_devices["DSManager"]["proxy"].subscribe_event(
                "achievedPointing",
                EventType.CHANGE_EVENT,
                partial(self.achieved_pointing_change_event_callback, "DSManager"),
            )

        time_elapsed = 0
        try:
            while True:
                # Wait
                time.sleep(.5)  # Sleep to keep the script running
                time_elapsed += .5

                # Output progress
                dm_az_updates = len(self.tango_devices["DishManager"]["az_values"])
                dm_el_updates = len(self.tango_devices["DishManager"]["el_values"])
                dsm_az_updates = len(self.tango_devices["DSManager"]["az_values"])
                dsm_el_updates = len(self.tango_devices["DSManager"]["el_values"])

                print(
                    f"\rTime elapsed ({time_elapsed}s) || "
                    f"DSC(Az: {len(self.opcua_az_values)}, El: {len(self.opcua_el_values)}) "
                    f"|| DishManager(Az: {dm_az_updates}, El: {dm_el_updates}) "
                    f"|| DSManager(Az: {dsm_az_updates}, El: {dsm_el_updates})",
                    end="",
                    flush=True,
                )
        except KeyboardInterrupt:
            print("\nScript interrupted. Unsubscribing...")
            if self.tango_devices["DishManager"]["proxy"] is not None:
                self.tango_devices["DishManager"]["proxy"].unsubscribe_event(subscription_id_dm)
            if self.tango_devices["DSManager"]["proxy"] is not None:
                self.tango_devices["DSManager"]["proxy"].unsubscribe_event(subscription_id_dsm)
            if self.opcua_client is not None:
                self.opcua_client.disconnect()

    def draw_plots(self, axs):
        # Update plots based on the latest data
        # Plot the TANGO data from DishManager
        tango_az_values_dm = self.tango_devices["DishManager"]["az_values"]
        if len(tango_az_values_dm) > 0:
            # Plot the data with indices
            self.graph_a_data, = axs[0, 0].plot(range(len(tango_az_values_dm)), tango_az_values_dm, label="Azimuth")
            axs[0, 0].plot(
                len(tango_az_values_dm) - 1, tango_az_values_dm[-1], "ro"
            )  # Mark last point
            axs[0, 0].annotate(
                f"{tango_az_values_dm[-1]:.2f}", (len(tango_az_values_dm) - 1, tango_az_values_dm[-1])
            )

            tango_el_values_dm = self.tango_devices["DishManager"]["el_values"]
            axs[0, 0].plot(range(len(tango_el_values_dm)), tango_el_values_dm, label="Elevation")
            axs[0, 0].plot(
                len(tango_el_values_dm) - 1, tango_el_values_dm[-1], "ro"
            )  # Mark last point
            axs[0, 0].annotate(
                f"{tango_el_values_dm[-1]:.2f}", (len(tango_el_values_dm) - 1, tango_el_values_dm[-1])
            )

            # Plot the data with timestamps
            tango_az_timestamps_dm = self.tango_devices["DishManager"]["az_timestamps"]
            axs[0, 1].plot(tango_az_timestamps_dm, tango_az_values_dm, label="Azimuth")
            axs[0, 1].plot(tango_az_timestamps_dm[-1], tango_az_values_dm[-1], "ro")  # Mark last point
            axs[0, 1].annotate(
                f"{tango_az_values_dm[-1]:.2f}", (tango_az_timestamps_dm[-1], tango_az_values_dm[-1])
            )

            tango_el_timestamps_dm = self.tango_devices["DishManager"]["el_timestamps"]
            axs[0, 1].plot(tango_el_timestamps_dm, tango_el_values_dm, label="Elevation")
            axs[0, 1].plot(tango_el_timestamps_dm[-1], tango_el_values_dm[-1], "ro")  # Mark last point
            axs[0, 1].annotate(
                f"{tango_el_values_dm[-1]:.2f}", (tango_el_timestamps_dm[-1], tango_el_values_dm[-1])
            )

            axs[0, 0].legend()
            axs[0, 1].legend()

        # Plot the TANGO data from DSManager
        tango_az_values_dsm = self.tango_devices["DSManager"]["az_values"]
        if len(tango_az_values_dsm) > 0:
            # Plot the data with indices
            axs[1, 0].plot(range(len(tango_az_values_dsm)), tango_az_values_dsm, label="Azimuth")
            axs[1, 0].plot(
                len(tango_az_values_dsm) - 1, tango_az_values_dsm[-1], "ro"
            )  # Mark last point
            axs[1, 0].annotate(
                f"{tango_az_values_dsm[-1]:.2f}", (len(tango_az_values_dsm) - 1, tango_az_values_dsm[-1])
            )

            tango_el_values_dm = self.tango_devices["DSManager"]["el_values"]
            axs[1, 0].plot(range(len(tango_el_values_dm)), tango_el_values_dm, label="Elevation")
            axs[1, 0].plot(
                len(tango_el_values_dm) - 1, tango_el_values_dm[-1], "ro"
            )  # Mark last point
            axs[1, 0].annotate(
                f"{tango_el_values_dm[-1]:.2f}", (len(tango_el_values_dm) - 1, tango_el_values_dm[-1])
            )

            # Plot the data with timestamps
            tango_az_timestamps_dm = self.tango_devices["DSManager"]["az_timestamps"]
            axs[1, 1].plot(tango_az_timestamps_dm, tango_az_values_dsm, label="Azimuth")
            axs[1, 1].plot(tango_az_timestamps_dm[-1], tango_az_values_dsm[-1], "ro")  # Mark last point
            axs[1, 1].annotate(
                f"{tango_az_values_dsm[-1]:.2f}", (tango_az_timestamps_dm[-1], tango_az_values_dsm[-1])
            )

            tango_el_timestamps_dm = self.tango_devices["DSManager"]["el_timestamps"]
            axs[1, 1].plot(tango_el_timestamps_dm, tango_el_values_dm, label="Elevation")
            axs[1, 1].plot(tango_el_timestamps_dm[-1], tango_el_values_dm[-1], "ro")  # Mark last point
            axs[1, 1].annotate(
                f"{tango_el_values_dm[-1]:.2f}", (tango_el_timestamps_dm[-1], tango_el_values_dm[-1])
            )

            axs[1, 0].legend()
            axs[1, 1].legend()

        # Plot the OPCUA data from DSC
        if len(self.opcua_az_timestamps) > 0:
            # Plot the data with indices
            axs[2, 0].plot(range(len(self.opcua_az_values)), self.opcua_az_values, label="Azimuth")
            axs[2, 0].plot(
                len(self.opcua_az_values) - 1, self.opcua_az_values[-1], "ro"
            )  # Mark last point
            axs[2, 0].annotate(
                f"{self.opcua_az_values[-1]:.2f}",
                (len(self.opcua_az_values) - 1, self.opcua_az_values[-1]),
                )

            axs[2, 0].plot(range(len(self.opcua_el_values)), self.opcua_el_values, label="Elevation")
            axs[2, 0].plot(
                len(self.opcua_el_values) - 1, self.opcua_el_values[-1], "ro"
            )  # Mark last point
            axs[2, 0].annotate(
                f"{self.opcua_el_values[-1]:.2f}",
                (len(self.opcua_el_values) - 1, self.opcua_el_values[-1]),
            )

            # Plot the data with timestamps
            axs[2, 1].plot(self.opcua_az_timestamps, self.opcua_az_values, label="Azimuth")
            axs[2, 1].plot(
                self.opcua_az_timestamps[-1], self.opcua_az_values[-1], "ro"
            )  # Mark last point
            axs[2, 1].annotate(
                f"{self.opcua_az_values[-1]:.2f}",
                (self.opcua_az_timestamps[-1], self.opcua_az_values[-1]),
            )

            axs[2, 1].plot(self.opcua_el_timestamps, self.opcua_el_values, label="Elevation")
            axs[2, 1].plot(
                self.opcua_el_timestamps[-1], self.opcua_el_values[-1], "ro"
            )  # Mark last point
            axs[2, 1].annotate(
                f"{self.opcua_el_values[-1]:.2f}",
                (self.opcua_el_timestamps[-1], self.opcua_el_values[-1]),
            )

            axs[2, 0].legend()
            axs[2, 1].legend()

        # Adjust layout
        plt.tight_layout()

    def update_plots_values(self):
        # self.graph_a_data = axs[0, 0].plot(range(len(tango_az_values_dm)), tango_az_values_dm, label="Azimuth")
        tango_az_values_dm = self.tango_devices["DishManager"]["az_values"]
        self.graph_a_data.set_data(range(len(tango_az_values_dm)), tango_az_values_dm)

    def create_combined_plot(self):
        # Create a figure and subplots
        fig, axs = plt.subplots(3, 2, figsize=(12, 10))

        axs[0, 0].set_xlabel("Index")
        axs[0, 0].set_ylabel("achievedPointing (deg)")
        axs[0, 0].set_title("DishManager with indices")

        axs[0, 1].set_xlabel("Timestamp")
        axs[0, 1].set_ylabel("achievedPointing (deg)")
        axs[0, 1].set_title("DishManager achievedPointing with timestamps")

        axs[1, 0].set_xlabel("Index")
        axs[1, 0].set_ylabel("achievedPointing (deg)")
        axs[1, 0].set_title("DSManager with indices")

        axs[1, 1].set_xlabel("Timestamp")
        axs[1, 1].set_ylabel("achievedPointing (deg)")
        axs[1, 1].set_title("DSManager with timestamps")

        axs[2, 0].set_xlabel("Index")
        axs[2, 0].set_ylabel("P_Act (deg)")
        axs[2, 0].set_title("OPCUA with indices")

        axs[2, 1].set_xlabel("Timestamp")
        axs[2, 1].set_ylabel("P_Act (deg)")
        axs[2, 1].set_title("OPCUA with timestamps")

        self.draw_plots(axs)

        return fig, axs

    def show_graph(self):
        fix, axs = self.create_combined_plot()
        plt.show()

    def save_data(self, output_dir="graph_data", show_graph=False):
        os.makedirs(output_dir, exist_ok=True)

        # Save the tango data to files
        for device in self.tango_devices:
            az_timestamps_file = os.path.join(
                output_dir, f"tango_azimuth_timestamps_{device.lower()}.txt"
            )
            el_timestamps_file = os.path.join(
                output_dir, f"tango_elevation_timestamps_{device.lower()}.txt"
            )
            az_values_file = os.path.join(output_dir, f"tango_azimuth_values_{device.lower()}.txt")
            el_values_file = os.path.join(
                output_dir, f"tango_elevation_values_{device.lower()}.txt"
            )

            np.savetxt(az_timestamps_file, self.tango_devices[device]["az_timestamps"], fmt="%f")
            np.savetxt(el_timestamps_file, self.tango_devices[device]["el_timestamps"], fmt="%f")
            np.savetxt(az_values_file, self.tango_devices[device]["az_values"], fmt="%f")
            np.savetxt(el_values_file, self.tango_devices[device]["el_values"], fmt="%f")

        # Save the opcua data to files
        opcua_timestamp_az_file = os.path.join(output_dir, "opcua_azimuth_timestamps.txt")
        opcua_timestamp_el_file = os.path.join(output_dir, "opcua_elevation_timestamps.txt")
        azimuth_file = os.path.join(output_dir, "opcua_azimuths.txt")
        elevation_file = os.path.join(output_dir, "opcua_elevations.txt")

        np.savetxt(opcua_timestamp_az_file, self.opcua_az_timestamps, fmt="%f")
        np.savetxt(opcua_timestamp_el_file, self.opcua_el_timestamps, fmt="%f")
        np.savetxt(azimuth_file, self.opcua_az_values, fmt="%f")
        np.savetxt(elevation_file, self.opcua_el_values, fmt="%f")

        fix, axs = self.create_combined_plot()

        # Save the combined figure
        plt.savefig(os.path.join(output_dir, "combined-graphs.png"))

        if show_graph:
            plt.show()
        plt.close() 

        print("Data saved to files.")

    def achieved_pointing_change_event_callback(self, device_name, event):
        timestamp, azimuth, elevation = event.attr_value.value

        # Save Azimuth values if newer timestamp and value isn't the same as the last
        if (len(self.tango_devices[device_name]["az_timestamps"]) == 0) or (
            timestamp >= self.tango_devices[device_name]["az_timestamps"][-1] and
            azimuth != self.tango_devices[device_name]["az_values"][-1]
        ):
            self.tango_devices[device_name]["az_values"].append(azimuth)
            self.tango_devices[device_name]["az_timestamps"].append(timestamp)

        # Save Elevation values if newer timestamp and value isn't the same as the last
        if (len(self.tango_devices[device_name]["el_timestamps"]) == 0) or (
            timestamp >= self.tango_devices[device_name]["el_timestamps"][-1] and
            elevation != self.tango_devices[device_name]["el_values"][-1]
        ):
            self.tango_devices[device_name]["el_values"].append(elevation)
            self.tango_devices[device_name]["el_timestamps"].append(timestamp)

    def get_tai_timestamp_from_datetime(self, datetime_obj: datetime.datetime) -> float:
        """Convert a datetime object into a TAI timestamp."""
        source_timestamp_unix = datetime_obj.timestamp()
        source_timestamp_tai = Time(source_timestamp_unix, format="unix").tai.value

        return source_timestamp_tai

    def az_p_act_change_cb(self, node, val, data) -> None:
        """Update the azimuth actual pointing coordinate."""
        # print("Azimuth Actual Pointing Coordinate CHANGE ", val)

        source_timestamp = data.monitored_item.Value.SourceTimestamp
        source_timestamp_tai = self.get_tai_timestamp_from_datetime(source_timestamp)

        self.opcua_az_timestamps.append(source_timestamp_tai)
        self.opcua_az_values.append(val)

    def el_p_act_change_cb(self, node, val, data) -> None:
        """Update the azimuth actual pointing coordinate."""
        # print("Elevation Actual Pointing Coordinate CHANGE ", val)

        source_timestamp = data.monitored_item.Value.SourceTimestamp
        source_timestamp_tai = self.get_tai_timestamp_from_datetime(source_timestamp)

        self.opcua_el_timestamps.append(source_timestamp_tai)
        self.opcua_el_values.append(val)


class SubscriptionHandler:
    """The SubscriptionHandler is used to handle the data that is received for the subscription."""

    def __init__(
        self,
        datachange_callback=None,
        status_change_callback=None,
    ) -> None:  # type: ignore
        """
        Initialise the handler with the datachange callback.

        This method will be called when the Client received a data change message from the Server.
        """
        if datachange_callback is not None:
            self.datachange_notification = datachange_callback  # type: ignore

        if status_change_callback is not None:
            self.status_change_notification = status_change_callback  # type: ignore

    def datachange_notification(self, data) -> None:
        """Log data change notification."""
        print(data)


if __name__ == "__main__":
    # Check if TANGO_HOST environment variable is set
    if "TANGO_HOST" not in os.environ:
        raise EnvironmentError("TANGO_HOST environment variable is not set.")

    grapher = PointingGrapher()

    grapher.begin()
    grapher.save_data()
