from astropy.time import Time

def get_tai_from_unix_s(unix_s: float) -> float:
    """
    Calculate atomic time in seconds from unix time in seconds.

    :param unix_s: Unix time in seconds

    :return: atomic time (tai) in seconds
    """
    astropy_time_utc = Time(unix_s, format="unix")
    return astropy_time_utc.unix_tai
